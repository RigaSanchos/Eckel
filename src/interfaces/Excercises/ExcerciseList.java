package interfaces.Excercises;

/**
 * Exercise 9: (3) Create an inheritance hierarchy of Rodent: Mouse, Gerbil, Hamster,
 etc. In the base class, provide methods that are common to all Rodents, and override these
 in the derived classes to perform different behaviors depending on the specific type of
 Rodent. Create an array of Rodent, fill it with different specific types of Rodents, and call
 your base-class methods to see what happens.
 *Exercise 1: (1) Modify Exercise 9 in the previous chapter so that Rodent is an abstract
 class. Make the methods of Rodent abstract whenever possible.
 Exercise 2: (1) Create a class as abstract without including any abstract methods and
 verify that you cannot create any instances of that class.
 Exercise 3: (2) Create a base class with an abstract print( ) method that is overridden
 in a derived class. The overridden version of the method prints the value of an int variable
 defined in the derived class. At the point of definition of this variable, give it a nonzero value.
 In the base-class constructor, call this method. In main( ), create an object of the derived
 type, and then call its print( ) method. Explain the results.
 Exercise 4: (3) Create an abstract class with no methods. Derive a class and add a
 method. Create a static method that takes a reference to the base class, downcasts it to the
 derived class, and calls the method. In main( ), demonstrate that it works. Now put the
 abstract declaration for the method in the base class, thus eliminating the need for the
 downcast.
 * Exercise 5: (2) Create an interface containing three methods, in its own package.
 Implement the interface in a different package.
 Exercise 6: (2) Prove that all the methods in an interface are automatically public.
 Exercise 7: (1) Change Exercise 9 in the Polymorphism chapter so that Rodent is an
 interface.
 Exercise 8: (2) In polymorphism.Sandwich.java, create an interface called
 FastFoo d (with appropriate methods) and change Sandwic h so that it also implements
 FastFood.
 Exercise 9: (3) Refactor Musics.java by moving the common methods in Wind,
 Percussion and Stringed into an abstract class.
 Exercise 10: (3) Modify Musics.java by adding a Playable interface. Move the play( )
 declaration from Instrument to Playable. Add Playable to the derived classes by
 including it in the implement s list. Change tune( ) so that it takes a Playable instead of
 an Instrument.
 * Exercise 11: (4) Create a class with a method that takes a String argument and produces
 a result that swaps each pair of characters in that argument. Adapt the class so that it works
 with interfaceprocessor.Apply.process( ).
 * Exercise 12: (2) In Adventure.java, add an interface called CanClimb, following the
 form of the other interfaces.
 Exercise 13: (2) Create an interface, and inherit two new interfaces from that interface.
 Multiply inherit a third interface from the second two.
 *Exercise 14: (2) Create three interfaces, each with two methods. Inherit a new interface
 that combines the three, adding a new method. Create a class by implementing the new
 interface and also inheriting from a concrete class. Now write four methods, each of which
 takes one of the four interfaces as an argument. In main( ), create an object of your class
 and pass it to each of the methods.
 Exercise 15: (2) Modify the previous exercise by creating an abstract class and
 inheriting that into the derived class.
 *Exercise 16: (3) Create a class that produces a sequence of chars. Adapt this class so
 that it can be an input to a Scanner object.
 * Exercise 17: (2) Prove that the fields in an interface are implicitly static and final.
 * Exercise 18: (2) Create a Cycle interface, with implementations Unicycle, Bicycle
 and Tricycle. Create factories for each type of Cycle, and code that uses these factories.
 Exercise 19: (3) Create a framework using Factory Methods that performs both coin
 tossing and dice tossing.
 * Created by alex on 03.08.16.
 */
public class ExcerciseList {
}
