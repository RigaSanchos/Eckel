package interfaces.Excercises;

/**
 * Exercise 9: (3) Create an inheritance hierarchy of Rodent: Mouse, Gerbil, Hamster,
 etc. In the base class, provide methods that are common to all Rodents, and override these
 in the derived classes to perform different behaviors depending on the specific type of
 Rodent. Create an array of Rodent, fill it with different specific types of Rodents, and call
 your base-class methods to see what happens.
 *Exercise 1: (1) Modify Exercise 9 in the previous chapter so that Rodent is an abstract
 class. Make the methods of Rodent abstract whenever possible.
 * Created by alex on 03.08.16.
 */
abstract class Rodent{
    abstract void searchingFood();

    abstract void eat();
    void run(){
        System.out.println("Rodent is runing");
    }
}

class Mouse extends Rodent{
    void searchingFood(){
        System.out.println("Mouse found food");
    }
    void eat(){
        System.out.println("Mouse eating food");
    }
    void run(){
        System.out.println("Mouse running");
    }
}

class Gerbill extends Rodent{
    void searchingFood(){}
    void eat(){}
    void run(){
        super.run();
    }
}

class Hamster extends Rodent{
    void searchingFood(){
        System.out.println("Hamster searching food");
    }
    void eat(){
        System.out.println("Hamster eat food");
    }
    void run(){
        System.out.println("Hamster is run");
    }
}
public class Excercise1 {
    public static void main(String[] args) {
        Rodent mouse1 = new Mouse();
        mouse1.searchingFood();
        mouse1.eat();
        mouse1.run();
        Rodent hamster = new Hamster();
        hamster.run();
        hamster.eat();
        Gerbill gerbill = new Gerbill();
        gerbill.run();
        gerbill.searchingFood();
        gerbill.eat();
    }
}
