package interfaces.Excercises;

/**Exercise 3: (2) Create a base class with an abstract print( ) method that is overridden
 in a derived class. The overridden version of the method prints the value of an int variable
 defined in the derived class. At the point of definition of this variable, give it a nonzero value.
 In the base-class constructor, call this method. In main( ), create an object of the derived
 type, and then call its print( ) method. Explain the results.
 * Created by alex on 03.08.16.
 */
abstract class Bird{
    abstract void print();
    Bird(){print();}
}
class Eagle extends Bird{
    int variable = 5;
    void print(){
        System.out.println(variable);
    }
}
public class Excercise2 {
    public static void main(String[] args) {
        Eagle eagle = new Eagle();
    }
}
