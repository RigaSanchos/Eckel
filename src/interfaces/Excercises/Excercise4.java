package interfaces.Excercises;

/** Exercise 4: (3) Create an abstract class with no methods. Derive a class and add a
 method. Create a static method that takes a reference to the base class, downcasts it to the
 derived class, and calls the method. In main( ), demonstrate that it works. Now put the
 abstract declaration for the method in the base class, thus eliminating the need for the
 downcast.
 Непонятно что надо сделать
 * Created by alex on 03.08.16.
 */
abstract class Insect{

}

class Bugg extends Insect{
    void method(){
        System.out.println("Method started");
    }
    static void staticMethod(Insect insect){

    }
}
public class Excercise4 {
}
