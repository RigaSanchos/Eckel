package interfaces.interfaceprocessor;

/**
 * Created by alex on 27.07.16.
 */
public interface Processor {
    String name();
    Object process(Object input);
}
