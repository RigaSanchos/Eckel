package interfaces.interfaceprocessor;
import static net.mindview.Print.*;
/**
 * Created by alex on 27.07.16.
 */
public class Apply {
    public static void process(Processor p, Object s){
        print("Using Processor " + p.name());
        print(p.process(s));
    }
}
