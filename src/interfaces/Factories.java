package interfaces;
import static net.mindview.Print.*;
/**
 * Created by alex on 03.08.16.
 */
interface Service{
    void method1();
    void method2();
}

interface ServiceFactory{
    Service getService();
}

class Implementation implements Service{
    Implementation(){}
    public void method1(){print("Implementation method 1");}
    public void method2(){print("Implementation method 2");}
}

class ImplementationFactory implements ServiceFactory{
    public Service getService(){
        return new Implementation();
    }
}

class Implementation2 implements Service{
    Implementation2(){}
    public void method1(){print("Implementation2 method1");}
    public void method2(){print("Implementation2 method2");}

}

class Implementation2Factory implements ServiceFactory{
    public Service getService(){
        return new Implementation2();
    }
}

public class Factories {
    public static void serviceConsumer(ServiceFactory fact){
        Service s = fact.getService();
        s.method1();
        s.method2();
    }

    public static void main(String[] args) {
        serviceConsumer(new ImplementationFactory());
        serviceConsumer(new Implementation2Factory());
    }
}
