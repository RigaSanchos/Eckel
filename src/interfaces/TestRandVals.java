package interfaces;
import static net.mindview.Print.*;
/**
 * Created by alex on 02.08.16.
 */
public class TestRandVals {
    public static void main(String[] args) {
        print(RandVals.RANDOM_INT);
        print(RandVals.RANDOM_LONG);
        print(RandVals.RANDOM_FLOAT);
        print(RandVals.RANDOM_DOUBLE);
    }
}
