package interfaces.filters;

/**
 * Created by alex on 27.07.16.
 */
public class BandPass extends Filter{
    double lowCutoff, highCutoff;
    public BandPass(double lowCut, double highCut){
        lowCutoff = lowCut;
        highCutoff = highCut;
    }
    public Waveform process(Waveform input){return input;}
}
