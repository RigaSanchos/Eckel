package interfaces.filters;

/**
 * Created by alex on 26.07.16.
 */
public class Filter {
    public String name(){
        return getClass().getSimpleName();
    }
    public Waveform process(Waveform input){
        return input;
    }
}

