package polymorphysm.music4;
import polymorphysm.music.Note;
import static net.mindview.Print.*;
/**
 * Created by alex on 24.07.16.
 */
abstract class Instrument{
    private int i;
    public abstract void play(Note n);
    public String what(){return "Instrument";}
    public abstract void adjust();
}

class Wind extends Instrument{
    public void play(Note n){
        print("Wind play() " + n);
    }
    public String what(){return "Wind";}
    public void adjust(){}
}

class Percussion extends Instrument{
    public void play(Note n){
        print("Perrcussion play() " + n);
    }
    public String what(){return "Percussion";}
    public void adjust(){}
}
class Stringed extends Instrument {
    public void play(Note n) {
        print("Stringed play() " + n);
    }

    public String what() {
        return "Stringed";
    }

    public void adjust() {
    }
}
class Brass extends Wind{
    public void play(Note n){
        print("Brass.play() " + n);
    }
    public void adjust(){print("Brass adjust()");}
}
class WoodWind extends Wind{
    public void play(Note n){
        print("Brass.play()" + n);
    }
    public String what(){return "WoodWind";}
}
public class Music4 {
    static void tune(Instrument i){
        i.play(Note.MIDDLE_C);
    }
    static void tuneAll(Instrument[] e){
        for(Instrument i : e)
            tune(i);
    }

    public static void main(String[] args) {
        Instrument[] orcestra = {
                new Wind(),
                new Percussion(),
                new Stringed(),
                new Brass(),
                new WoodWind()
        };
        tuneAll(orcestra);
    }

}
