package polymorphysm;
import static net.mindview.Print.*;
/**
 * Created by alex on 24.07.16.
 */
class Actor{
    public void act(){}
}
class HappyActor extends Actor{
    public void act(){print("Happy Actor");}
}
class SadActor extends Actor{
    public void act(){print("SadActor");}
}
class Stage{
    private Actor actor = new HappyActor();
    public void change(){actor = new SadActor();}
    public void performPlay(){actor.act();}
}
public class Transmogrify {
    public static void main(String[] args) {
        Stage stage = new Stage();
        stage.performPlay();
        stage.change();
        stage.performPlay();
    }
}
