package innerclasses;

/**
 * Created by alex on 04.08.16.
 */
public interface Destination {
    String readLabel();
}
