package innerclasses;

/**
 * Created by alex on 04.08.16.
 */
class Parcel4{
    private class PContents implements Contents{
        private int i = 11;
        public int value(){return i;}
    }
    public class PDestination implements Destination{
        private String label;
        private PDestination(String whereTo){label = whereTo;}
        public String readLabel(){return label;}
    }
    public Destination destination(String s){
        return new PDestination(s);
    }
    public Contents contents(){
        return new PContents();
    }
}
public class TheParcel {
    public static void main(String[] args) {
        Parcel4 p = new Parcel4();
        Contents c = p.contents();
        Destination d = p.destination("Tasmania");

    }
}
