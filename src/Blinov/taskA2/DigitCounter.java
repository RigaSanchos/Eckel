package Blinov.taskA2;

/**
 * Created by alex on 23.10.16.
 */
class DigitCounter{
    public int digitCount(int number){
        int counter = 1;
        while ((number/=10)%10>0){
            counter++;
        }
        return counter;
    }
}
