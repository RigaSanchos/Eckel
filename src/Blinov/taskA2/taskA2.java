package Blinov.taskA2;

import java.util.Date;
import java.util.Scanner;


/**
 * В приведенных ниже заданиях необходимо вывести внизу фамилию разра-
 ботчика, дату и время получения задания, а также дату и время сдачи задания.
 Добавить комментарии в программы в виде  комментарий , сгенерировать
        html-файл документации. В заданиях на числа объект можно создавать в виде
        массива символов.
        Ввести n чисел с консоли.
        1.	 Найти самое короткое и самое длинное число. Вывести найденные числа
        и их
        Created by alex on 17.10.16.
 **/
public class taskA2 {

    public static void main(String[] args) {
        int longestNumber = 0, shortestNumber = Integer.MAX_VALUE;
        System.out.println("Enter number of numbers");
        Scanner scan = new Scanner(System.in);
        int numberCount = scan.nextInt();
//        System.out.println(numberCount);

        int [] intArray = new int[numberCount];

//        System.out.println(intArray.length);
        DigitCounter counter = new DigitCounter();
        Sorter sorter = new Sorter();
        for (int i = 0; i < intArray.length; i++)
        {
//            System.out.print(intArray[i] + " ");
            intArray[i] = scan.nextInt();
            if (counter.digitCount(intArray[i])<counter.digitCount(shortestNumber)) shortestNumber = intArray[i];
            if (counter.digitCount(intArray[i])>counter.digitCount(longestNumber)) longestNumber = intArray[i];
        }



        System.out.println("The shortest number is: " + shortestNumber);
        System.out.println("The longest number is: " + longestNumber);
        System.out.println("-----------------------------");
        System.out.println("Author: Aleksandr Melnychenko");
        System.out.println("Date 12 10 2016");
        System.out.println(new Date());
        scan.close();

    }



}
