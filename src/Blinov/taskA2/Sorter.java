package Blinov.taskA2;

/**
 * Created by alex on 23.10.16.
 */
public class Sorter {
    DigitCounter counter = new DigitCounter();
    int [] sort(int [] array){
        int temp = 0;
        for (int i = 0; i < array.length; i++){
            for (int j = array.length-1; j > i; j--){
                if (counter.digitCount(array[i]) > counter.digitCount(array[j])){
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }
}
