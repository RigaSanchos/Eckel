package Blinov.taskA2.Airplane;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**Создать объект класса Самолет, используя классы Крыло, Шасси,
 Двигатель. Методы: летать, задавать маршрут, вывести на консоль маршрут.
 * Created by alex on 29.10.16.
 */
public class Airplane {
    String departure;
    String destination;
    static String route;
    Scanner scan = new Scanner(System.in);
    private List<Wing> wings;
    private List<Chasis> chasis;
    private List<Engine> engines;

    Airplane(){
        wings = new ArrayList<>();
        for (int i = 0; i < 2; i++) wings.add(new Wing());
        chasis = new ArrayList<>();
        for (int i = 0; i < 8; i++) chasis.add(new Chasis());
        engines = new ArrayList<>();
        for (int i = 0; i < 4; i++) engines.add(new Engine("Reactive", "Boing", 700));

    }

    void fly(){
        for (Engine e : engines){
            e.turnOn();
            e.toString();
        }

        for (Wing w : wings){
            w.liftFlaps();
        }

        for (Chasis c : chasis){
            c.chasisUp();
        }

       System.out.printf("Trne airplane flying %s \n", route);

        for(Chasis c : chasis) {
            c.chasisDown();
        }

        for (Wing w : wings){
            w.liftFlaps();
        }

        for(Engine e : engines){
            e.turnOff();
        }


    }


    String setRoute(String destination, String departure){
        System.out.println("Please enter the departure point");
       // departure = scan.nextLine();
        System.out.println("enter the destination point");
        //destination = scan.nextLine();
        String route = "from " + departure + " to " + destination ;
        System.out.println(route);
        return this.route = route;
    }

    void routeToConsole(){
        System.out.println("The plane rote is:" + route);
    }

    @Override
    public String toString(){
        return "Planeine is " + engines.toString();
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if ((obj == null) || (getClass()!=obj.getClass())) return false;
        Airplane plane1 = (Airplane) obj;
        return route.equalsIgnoreCase(plane1.route);
    }
}
