package Blinov.taskA2.Airplane;

/**
 * Created by alex on 29.10.16.
 */
public class Chasis {
    int radius;
    int pressure;
    boolean chasisUp = false;

    void chasisUp(){
        chasisUp=true;
    }

    void chasisDown(){
        chasisUp = false;
    }

    @Override
    public String toString(){
       return "pressure = " + pressure + "radius" + radius + (chasisUp ? "Chasis Upped" : "Chasis downed");
    }

    @Override
    public int hashCode(){
        final int prime = 61;
        int result = 1;
        result = prime*result  + prime*radius + prime * pressure + (chasisUp ? 1 : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) return  true;
        if (obj == null||obj.getClass()!=this.getClass()) return false;
        Chasis chasis = (Chasis) obj;
        return radius==chasis.radius && (pressure == chasis.pressure && (chasisUp==chasis.chasisUp));
    }
}
