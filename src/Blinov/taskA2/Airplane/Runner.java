package Blinov.taskA2.Airplane;

/**Создать приложение, удовлетворяющее требованиям, приведенным в зада-
 нии. Наследование применять только в тех заданиях, в которых это логически
 обосновано. Аргументировать принадлежность классу каждого создаваемого
 метода и  корректно переопределить для каждого класса методы equals(),
 hashCode(), toString().
 Создать объект класса Самолет, используя классы Крыло, Шасси,
 Двигатель. Методы: летать, задавать маршрут, вывести на консоль маршрут.
 @author Oleksandr Melnychenko
 * Created by alex on 29.10.16.
 */
public class Runner {
    public static void main(String[] args) {
        Airplane plane = new Airplane();
        plane.setRoute("Bangkok", "Singapoore");
        plane.routeToConsole();
        plane.fly();
    }
}
