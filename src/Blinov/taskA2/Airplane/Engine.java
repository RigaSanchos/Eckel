package Blinov.taskA2.Airplane;

/**
 * Created by alex on 30.10.16.
 */
public class Engine {
    boolean turnedOn;
    final private String type;
    final private String brand;
    final int power;


    public Engine(String type, String brand, int power){
        this.type = type;
        this.brand = brand;
        turnedOn = false;
        this.power = power;
    }

    void turnOn(){
        System.out.println("Engine turned on");
        turnedOn = true;
    }
    void turnOff(){
        System.out.println("Egine turned off");
        turnedOn = false;

    }

    String getBrang(){
        return brand;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("{ ");
        sb.append(type);
        sb.append(brand);
        sb.append(turnedOn);
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Engine eng = (Engine) obj;
        return power==eng.power && (type.equalsIgnoreCase(eng.type)||brand.equalsIgnoreCase(eng.brand)||turnedOn!=eng.turnedOn);
    }

    @Override
    public int hashCode(){
        int result = 61*power + (turnedOn ? 0 : 1) + ((brand == null ? 0 : brand.hashCode()) + (type == null ? 0 : type.hashCode()));
        return result;
    }

}
